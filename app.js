const express = require('express');
const user_gameRoutes = require('./control/routes');
const app = express();
app.set('view engine', 'ejs');
const port = 3000;

app.use(express.json());
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.send("Hello Word!");
});

app.get("/login",(req, res) =>{
  res.render("login_user");
});

app.get("/game", (req,res)=>{
  res.render("game_user");
});

app.use('/api/v1', user_gameRoutes);
app.listen(port, () => console.log(`app listening on port ${port}`));