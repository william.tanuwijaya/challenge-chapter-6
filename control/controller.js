const pool = require('./db');
const queries = require('./queries');
const { get } = require('./routes');

const getUser_name = (req, res) =>{
  pool.query(queries.getUser_name,(error, results) =>{
    if(error) throw error;
    res.status(200).json(results.rows);
  });
  
};
const addUser_game = (req, res) => {
  console.log(req.body.username)
  pool.query(queries.addUser_game(req.body.id, req.body.username, req.body.password), (error, results) =>{
    if(error) throw error;
    res.status(200).json({"message":"success added data"});
  })
}

const editUser_game = (req,res)=>{
  pool.query(queries.editUser_game(req.params.id, req.body.username, req.body.password), (error, results) =>{
    if(error) throw error;
    res.status(200).json({"messange":"edit"});
  })
}

const deleteUser_game = (req,res) =>{
  pool.query(queries.deleteUser_game(req.params.id), (error, results) =>{
    if(error) throw error;
    res.status(200).json({"messange":"delete data successfully"});
  })
}

const submitScore = (req,res) =>{
  pool.query(queries.submit_Score(req.body.score), (error, results) => {
    if(error) throw error;
    res.status(200).json({"messange":"score successfully"});
  })
}

module.exports = {
  getUser_name, addUser_game, editUser_game, deleteUser_game, submitScore
};
