const { Router } = require('express');
const controller = require('./controller');

const router = Router();

router.get("/user_game", controller.getUser_name);
router.put("/user_game", controller.editUser_game);
router.delete("/user_game/:id", controller.deleteUser_game);
router.post("/user_game", controller.addUser_game);
router.post("/submit-score", controller.submitScore);

module.exports = router;